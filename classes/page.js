export default class Page {
  constructor(title, layout, render_product, render_category) {
    this.layout = layout;
    this.title = title;
    this.render_product = render_product;
    this.render_category = render_category;
  }
}
