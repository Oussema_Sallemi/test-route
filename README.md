# test-route

## after a link is clicked,the middleware will :

## check if the route chosen is in the database or not, if not it will throw a custom Nuxt JS error 404 page

## if it exists it will check the route type (product/category) from the PATHS API then it will populate the page render configuration state with the appropriate configurations

## the page_config state will be delivered to the page via vuex store for the page to render the appropriate layout

## after the layout is set the page will call the appropriate API (CATEGORIES/PRODUCTS) to populate the layout specific componenets via props

## the page will render dynamically

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
