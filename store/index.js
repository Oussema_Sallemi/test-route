export const state = () => ({
  products: [],
  categories: [],
  upward_data: [],
  /* page configuration state that will be populated from the middleware to diliver the render configurations to the page */
  page_config: {
    layout: "",
    title: "",
    render_product: false,
    render_category: false
  }
});
export const getters = {
  getProducts: state => {
    return state.products;
  },
  getCategories: state => {
    return state.categories;
  },
  getUpwardData: state => {
    return state.upward_data;
  }
};
export const mutations = {
  SET_PRODUCTS: (state, payload) => {
    state.products = payload;
  },
  SET_CATEGORIES: (state, payload) => {
    state.categories = payload;
  },
  SET_UPWARD_DATA: (state, payload) => {
    state.upward_data = payload;
  },
  /* this mutation will populate the page_config state with the appropriate datet coming from the middleware */
  CHANGE_PAGE_CONFIG(state, payload) {
    state.page_config.title = payload.title;
    state.page_config.layout = payload.layout;
    state.page_config.render_product = payload.render_product;
    state.page_config.render_category = payload.render_category;
  }
};
export const actions = {
  async nuxtServerInit({ commit }) {
    const prods = await this.$axios.$get("http://localhost:5000/products");
    commit("SET_PRODUCTS", prods);
    const categs = await this.$axios.$get("http://localhost:5000/categories");
    commit("SET_CATEGORIES", categs);
    const upward = await this.$axios.$get(
      "http://localhost:5000/graphql?query={category(id: 2020){products{total_count}}}"
    );
    commit("SET_UPWARD_DATA", upward);
  }
};
