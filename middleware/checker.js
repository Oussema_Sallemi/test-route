import axios from "axios";
import Page from "../classes/page";
/* all data and configs will be sent from the middleware to the render page via vuex store */
export default async function({ route, store, error }) {
  /* checks route against index */
  switch (route.path) {
    case "/":
      const index_page = new Page("", "", false, false);
      store.commit("CHANGE_PAGE_CONFIG", index_page);
      break;
    /* if route is not index, it will check the PATHS API to see in it exists in the paths database */
    default:
      const route_exists = await axios
        .get(`http://localhost:5000/paths${route.path}`)
        .then(res => {
          return res.data;
        })
        .catch(err => {
          console.log(err);
        });
      /* if the response of the API indicates that the route doesn't exist, than the app will throw a custom Nuxt 404 page */
      switch (route_exists) {
        case "route not found!":
          const error_page = new Page("", "", false, false);
          store.commit("CHANGE_PAGE_CONFIG", error_page);
          error({
            statusCode: 404,
            message: "Oops,Page not found!"
          });
          break;
        /* if the route exists, than we will check what is the type of that route (product/category) */
        /* the route object in the database contains the route url and the type of the route */
        default:
          switch (route_exists.type) {
            /* if the route is a product route,the middleware will borker the product page configuration to display the product details */
            case "product":
              const product_page = new Page(
                await axios
                  .get(`http://localhost:5000/products${route.path}`)
                  .then(res => {
                    return res.data.name;
                  })
                  .catch(err => {
                    console.log(err);
                  }),
                "product-layout",
                !store.state.page_config.render_product,
                false
              );
              store.commit("CHANGE_PAGE_CONFIG", product_page);
              break;
            /* if the route is a category route,the middleware will borker the category page configuration to display the category details */
            case "category":
              const category_page = new Page(
                await axios
                  .get(`http://localhost:5000/categories${route.path}`)
                  .then(res => {
                    return res.data.name;
                  })
                  .catch(err => {
                    console.log(err);
                  }),
                "category-layout",
                false,
                !store.state.page_config.render_category
              );
              store.commit("CHANGE_PAGE_CONFIG", category_page);
              break;
            /* if the route has no known type, the middlware will throw an error log message */
            default:
              console.log("unidentified route type!");
              break;
          }
          break;
      }

      break;
  }
}
