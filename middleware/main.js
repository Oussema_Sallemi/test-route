import Page from "../classes/page";
export default async function({ route, store, redirect }) {
  if (decodeURI(route.path).replace("/", "") === "") {
    console.log("this is index!");
  } else if (
    store.state.users.some(u => {
      return decodeURI(route.path).replace("/", "") === u.name;
    })
  ) {
    const user_page = new Page(
      store.state.users.filter(u => {
        {
          return decodeURI(route.path).replace("/", "") === u.name;
        }
      })[0].name,
      "user-layout",
      !store.state.render_user,
      false
    );
    store.commit("CHANGE_PAGE_CONFIG", user_page);
  } else if (
    store.state.photos.some(p => {
      return decodeURI(route.path).replace("/", "") === p.title;
    })
  ) {
    const photo_page = new Page(
      store.state.photos.filter(p => {
        {
          return decodeURI(route.path).replace("/", "") === p.title;
        }
      })[0].title,
      "photo-layout",
      false,
      !store.state.render_photo
    );
    store.commit("CHANGE_PAGE_CONFIG", photo_page);
  } else {
    // this is the default 404 page
    //redirect(404);
    redirect("/error");
  }
}
